const Promise = require('promise');
const moment = require('moment');
const Temperature = require('../../database/models/temperature.schema.js');
const State = require('../../database/models/state.schema.js');
const Config = require('../../database/models/config.schema');

//const raspberryService = require('../../raspberryService.js');
let mainRepository = function (io) {
    let getSensorsData = function () {
        return new Promise(function (resolve, reject) {
            State.find({}, function (error, states) {
                if (error) {
                    console.log(error);
                    reject(error);
                }
                resolve({
                    sensors: states[0]
                });
            });
        });
    };

    let getTemperaturesData = function () {
        return new Promise(function (resolve, reject) {
            let lowerDate = new Date();
            lowerDate.setDate(lowerDate.getDate() - 1);

            Temperature.find({"date": {"$gte": lowerDate}}).sort('date').exec(function (error, temperatures) {
                if (error) {
                    console.log(error);
                    reject(error);
                }
                resolve({
                    lastTemperatureValues: temperatures
                });
            });
        });
    };

    let getSystemData = function () {
        return new Promise(function (resolve, reject) {
            let system = {
                status: global.socketConnected
            };
            resolve(system);
        });
    };

    let feed = function (options) {
        return new Promise(function (resolve, reject) {
            let status;
            io.emit('feed');
            status = true;
            resolve(status);
        });
    };

    let toggleLight = function (options) {
        return new Promise(function (resolve, reject) {
            let status;
            if (options.newLightState) {
                io.emit('light', 1);
                status = 1;
            } else {
                io.emit('light', 0);
                status = 0;
            }

            resolve(status);
        });
    };

    let toggleHeater = function (options) {
        return new Promise(function (resolve, reject) {
            let status;
            if (options.newHeaterState) {
                io.emit('heater', 1);
                status = 1;
            } else {
                io.emit('heater', 0);
                status = 0;
            }

            resolve(status);
        });
    };

    let toggleFilter = function (options) {
        return new Promise(function (resolve, reject) {
            let status;
            if (options.newFilterState) {
                io.emit('filter', 1);
                status = 1;
            } else {
                io.emit('filter', 0);
                status = 0;
            }

            resolve(status);
        });
    };

    let getConfigData = function () {
        return new Promise(function (resolve, reject) {
            Config.findOne({}, function (err, foundConfig) {
                if (err || !foundConfig) {
                    console.log(err);
                    reject(err);
                }

                resolve(foundConfig);
            });
        });
    };

    let setConfigData = function (newConfigData) {
        return new Promise(function (resolve, reject) {
            Config.findByIdAndUpdate(newConfigData._id, newConfigData, function (err, updatedConfig) {
                if (err || !updatedConfig) {
                    console.log(err);
                    reject(err);
                }

                io.emit('updateConfigData');
                resolve(updatedConfig);
            });
        });
    };

    return {
        getSensorsData: getSensorsData,
        getSystemData: getSystemData,
        toggleLight: toggleLight,
        toggleHeater: toggleHeater,
        feed: feed,
        toggleFilter: toggleFilter,
        getTemperaturesData: getTemperaturesData,
        getConfigData: getConfigData,
        setConfigData: setConfigData
    };
};

module.exports = mainRepository;

