const rest = require('../rest.helper');

let mainRoutes = function (router, io) {
    const mainRepository = require('../repositories/main.repository')(io);

    router
        .route('/sensors')
        .get(function (req, res) {
            mainRepository.getSensorsData().then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });

    router
        .route('/config')
        .get(function (req, res) {
            mainRepository.getConfigData().then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        })
        .post(function (req, res) {
            let newConfigData = req.body;

            mainRepository.setConfigData(newConfigData).then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });

    router
        .route('/temperatures')
        .get(function (req, res) {
            mainRepository.getTemperaturesData().then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });

    router
        .route('/sensors/feeding')
        .put(function (req, res) {
            let options = req.body;

            mainRepository.feed(options).then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });

    router
        .route('/sensors/light')
        .put(function (req, res) {
            let options = req.body;

            mainRepository.toggleLight(options).then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });

    router
        .route('/sensors/heater')
        .put(function (req, res) {
            let options = req.body;

            mainRepository.toggleHeater(options).then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });

	router
        .route('/sensors/filter')
        .put(function (req, res) {
            let options = req.body;

            mainRepository.toggleFilter(options).then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        });


    router
        .route('/system')
        .get(function (req, res) {
            mainRepository.getSystemData().then((response) => {
                rest.Ok(res, response, 200);
            }, (error) => {
                rest.Error(res, 'ERROR', [error], 404);
            });
        })
        .post(function (req, res) {
            rest.Ok(res, {ok: true}, 200);
        });

};

module.exports = mainRoutes;
