const sonda = require('ds18b20');
const DS18B20_SONDA = '28-03168c330bff';
const Gpio = require('pigpio').Gpio;
const light = new Gpio(26, {mode: Gpio.OUTPUT});
const heater = new Gpio(27, {mode: Gpio.OUTPUT});
const filter = new Gpio(22, {mode: Gpio.OUTPUT});
const motor = new Gpio(18, {mode: Gpio.OUTPUT});
	
const Promise = require('promise');

function runServoMotor(iterationCount) {
	return new Promise(function (resolve, reject) {
		let pulseWidth = 1000;
		let increment = 100;
		let count = 0;
		
		console.log('[' + logDateTime() + '| RPI SERVO:] Servo runned, iterCount: ', iterationCount);
	
		iterationCount = iterationCount? iterationCount: 100;
		let interval = setInterval(function () {
			  motor.servoWrite(pulseWidth);
			  pulseWidth += increment;
			  
			  count++;
			  
			  if (pulseWidth >= 2000) {
				increment = -100;
			  } else if (pulseWidth <= 500) {
				increment = 100;
			  }
			  
			  if(count == iterationCount) {
				clearInterval(interval);
				resolve({
					status: 'ok'
				});
			  }
			}, 10);
		});
}

function getCurrentTemperatureWithoutPromise() {
	let temp = sonda.temperatureSync(DS18B20_SONDA);
	console.log('[' + logDateTime() + '| RPI DS18B20:] Current temperature read: ', temp);
	
	return temp;
}

function getCurrentTemperature() {
	return new Promise(function (resolve, reject) {
		let temp = sonda.temperatureSync(DS18B20_SONDA);
		console.log('[' + logDateTime() + '| RPI DS18B20:] Current temperature read: ', temp);
		
		resolve({
			temperature: temp,
			status: 'ok'
		});
	});
}

function switchLight(newLightState) {
	return new Promise(function (resolve, reject) {
		light.digitalWrite(newLightState? 0: 1);
		
		let newState = light.digitalRead();
		console.log('[' + new Date() + '| RPI GPIO_LIGHT:] Light toggled: ', newLightState);
		
		resolve({
			newState: newLightState,
			status: 'ok'
		});
	});
}

function checkLight() {
	console.log('[' + logDateTime() + '| RPI GPIO_LIGHT:] Light readed: ', light.digitalRead());
	return light.digitalRead()? 0: 1;
}

function switchHeater(newHeaterState) {
	return new Promise(function (resolve, reject) {
		heater.digitalWrite(newHeaterState? 0: 1);
		
		let newState = heater.digitalRead();
		console.log('[' + logDateTime()  + '| RPI GPIO_HEATER:] Heater toggled: ', newHeaterState);
		
		resolve({
			newState: newHeaterState,
			status: 'ok'
		});
	});
}

function checkHeater() {
	console.log('[' + logDateTime()  + '| RPI GPIO_HEATER:] Heater readed: ', heater.digitalRead());
	return heater.digitalRead()? 0: 1;
}

function switchFilter(newFilterState) {
	return new Promise(function (resolve, reject) {
		filter.digitalWrite(newFilterState? 0: 1);
		
		let newState = filter.digitalRead();
		console.log('[' + logDateTime()  + '| RPI GPIO_FILTER:] Filter toggled: ', newFilterState);
		
		resolve({
			newState: newFilterState,
			status: 'ok'
		});
	});
}

function checkFilter() {
	console.log('[' +logDateTime()  + '| RPI GPIO_FILTER:] Filter readed: ', filter.digitalRead());
	return filter.digitalRead()? 0: 1;
}


function logDateTime() {
	return new Date().toLocaleTimeString() + ' ' + new Date().toLocaleDateString();
}

module.exports = {
	getCurrentTemperature: getCurrentTemperature,
	getCurrentTemperatureWithoutPromise: getCurrentTemperatureWithoutPromise,
	runServoMotor: runServoMotor,
	switchLight: switchLight,
	checkLight: checkLight,
	switchHeater: switchHeater,
	checkHeater: checkHeater,
	switchFilter: switchFilter,
	checkFilter: checkFilter
}

