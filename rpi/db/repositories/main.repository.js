let Promise = require('promise');
let Temperature = require('../models/temperature.schema');
let Feeding = require('../models/feeding.schema');
let LightSwitching = require('../models/lightSwitching.schema');
let FilterSwitching = require('../models/filterSwitching.schema');
let HeaterSwitching = require('../models/heaterSwitching.schema');
let State = require('../models/state.schema');

let updateSensorsState = function (sensors) {
    return new Promise(function (resolve, reject) {
		
        State.find({}, function (error, states) {
            if (error) {
                reject(error);
            }
            
            if(states && states[0]) {
				State.findByIdAndUpdate(states[0]._id, sensors, function (error, state) {
					if (error) {
						reject(error);
					}    
					resolve(state._id);	
				});
			} else {
				let state = new State(sensors);
				state.save(function(error, states) {
					if (error) {
						reject(error);
					}    
					resolve(state._id);	
				});
			}
        });
    });
};

let saveTemperatureMeisurement = function (date, value, status, data) {
    return new Promise(function (resolve, reject) {
        let newTemperatureMeisurement = new Temperature({
            date: date,
            value: value,
            status: status,
            data: data
        });

        newTemperatureMeisurement.save(function (error, savedTemp) {
            if (error) {
                reject(error);
            }
            resolve(savedTemp._id);
        });
    });
};

let saveFeedingTime = function (date, status, data) {
    return new Promise(function (resolve, reject) {
        let newFeedingTime = new Feeding({
            date: date,
            status: status,
            data: data
        });
 
        newFeedingTime.save(function (error, savedFeedingTime) {
            if (error) {
                reject(error);
            }
            resolve(savedFeedingTime._id);
        });
    });
};

let saveLightSwitchingTime = function (date, status, data) {
    return new Promise(function (resolve, reject) {
        let newLightSwitchingTime = new LightSwitching({
            date: date,
            status: status,
            data: data
        });

        newLightSwitchingTime.save(function (error, savedLightSwitchingTime) {
            if (error) {
                reject(error);
            }
            resolve(savedLightSwitchingTime._id);
        });
    });
};

let saveFilterSwitchingTime = function (date, status, data) {
    return new Promise(function (resolve, reject) {
        let newFilterSwitchingTime = new FilterSwitching({
            date: date,
            status: status,
            data: data
        });

        newFilterSwitchingTime.save(function (error, savedFilterSwitchingTime) {
            if (error) {
                reject(error);
            }
            resolve(savedFilterSwitchingTime._id);
        });
    });
};

let saveHeaterSwitchingTime = function (date, status, data) {
    return new Promise(function (resolve, reject) {
        let newHeaterSwitchingTime = new HeaterSwitching({
            date: date,
            status: status,
            data: data
        });

        newHeaterSwitchingTime.save(function (error, savedHeaterSwitchingTime) {
            if (error) {
                reject(error);
            }
            resolve(savedHeaterSwitchingTime._id);
        });
    });
};

module.exports = {
    saveTemperatureMeisurement: saveTemperatureMeisurement,
    saveFeedingTime: saveFeedingTime,
    saveLightSwitchingTime: saveLightSwitchingTime,
    saveFilterSwitchingTime: saveFilterSwitchingTime,
    saveHeaterSwitchingTime: saveHeaterSwitchingTime,
    updateSensorsState: updateSensorsState
}
