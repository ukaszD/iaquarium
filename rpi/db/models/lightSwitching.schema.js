let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let lightSwitching = new Schema({
    date: Date,
    status: String,
    data: {}
});

lightSwitching.pre('save', function (next) {
    let error = null;

    //this.name ? error = null : error = new Error("Name missing");
    next(error);
});

let LightSwitching = mongoose.model('LightSwitching', lightSwitching);

module.exports = LightSwitching;
