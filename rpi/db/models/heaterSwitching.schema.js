let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let heaterSwitching = new Schema({
    date: Date,
    status: String,
    data: {}
});

heaterSwitching.pre('save', function (next) {
    let error = null;

    //this.name ? error = null : error = new Error("Name missing");
    next(error);
});

let HeaterSwitching = mongoose.model('HeaterSwitching', heaterSwitching);

module.exports = HeaterSwitching;
