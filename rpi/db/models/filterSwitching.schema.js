let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let filterSwitching = new Schema({
    date: Date,
    status: String,
    data: {}
});

filterSwitching.pre('save', function (next) {
    let error = null;

    //this.name ? error = null : error = new Error("Name missing");
    next(error);
});

let FilterSwitching = mongoose.model('FilterSwitching', filterSwitching);

module.exports = FilterSwitching;
