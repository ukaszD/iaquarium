let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let feeding = new Schema({
    date: Date,
    status: String,
    data: {}
});

feeding.pre('save', function (next) {
    let error = null;

    //this.name ? error = null : error = new Error("Name missing");
    next(error);
});

let Feeding = mongoose.model('Feeding', feeding);

module.exports = Feeding;
