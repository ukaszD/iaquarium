const express = require('express');
const mongoose = require('mongoose');
const appArguments = require('optimist').argv;
const serverHelper = require('../backend/server.helper');
const scheduler = require('node-schedule');
const config = require('../config');
const Config = require('./db/models/config.schema');
const io = require('socket.io-client');
const argv = require('optimist').argv;

let socketUrl = argv.socketUrl;
let socket = io(socketUrl);
let schedulers = [];
let configData = {};

socket.on('connect', function() {
	console.log('[' + logDateTime() + '| RPI:] socket connected! url: ' + socketUrl);
	socket.emit('rpiConnected');
	socket.emit('refresh');
});


// ------------- promise deprecation warning bypass -----------------------------
mongoose.Promise = global.Promise;

serverHelper.app.use('/scripts', serverHelper.express.static(__dirname + '/node_modules/'));

mongoose
	.connect(config.dbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
        if (err) {
            console.error('[' + logDateTime() + '| DATABASE:] Unable to connect to server... \n### Error:', err.message);
        } else {
            console.log('[' + logDateTime() + '| DATABASE:] Connection established to database at url: ', config.dbUrl);
            
            Config.findOne({}, function(err, foundConfigData) {
				if(err) {
					console.log('[' + logDateTime() + '| RPI:] Config data not found, ', err);
				}
				
				console.log('[' + logDateTime() + '| RPI:] Config data found' , err);
				
				configData = foundConfigData;
				setCroneIntervals(foundConfigData);   
				updateSensorsState(socket);
				registerSocketListeners(socket);
			});
        }
    });

//================================================================================

const raspberryService = require('./raspberryService');
const mainRepository = require('./db/repositories/main.repository');

function refreshCroneIntervals() {
	console.log('[' + logDateTime() + '| RPI:] Crone intervals refreshing...');
	for(let i=schedulers.length-1; i>=0; i--) {
		console.log('job ' + i);
		schedulers[i].cancel();
		schedulers.splice(i,1);
	}
	console.log('[' + logDateTime() + '| RPI:] Crone intervals canceled');
	
	Config.findOne({}, function(err, foundConfigData) {
		if(err) {
			console.log('[' + logDateTime() + '| RPI:] Config data not found, ', err);
		}
		
		configData = foundConfigData;
		setCroneIntervals(foundConfigData);  
	});
  
}

function setCroneIntervals(croneConfigData) {
	console.log('[' + logDateTime() + '| RPI:] Crone intervals setting... ');
	
	let tempReadIntervalCroneCmd = croneConfigData.tempReadIntervalCroneCmd;
	let saveTempJob = scheduler.scheduleJob(tempReadIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Read and save current temperature...');
		saveCurrentTemp();
	});
	schedulers.push(saveTempJob);
	
	let updateStateIntervalCroneCmd = croneConfigData.updateStateIntervalCroneCmd;
	let updateStateJob = scheduler.scheduleJob(updateStateIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Update status...');
		updateSensorsState();
	});
	schedulers.push(updateStateJob);

	let feedingIntervalCroneCmd = croneConfigData.feedingIntervalCroneCmd;
	let feedJob = scheduler.scheduleJob(feedingIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Feed and save feeding date...');
		feed();
	});
	schedulers.push(feedJob);
	
	let lightSwitchingOnIntervalCroneCmd = croneConfigData.lightSwitchingOnIntervalCroneCmd;
	let swichOnLightJob = scheduler.scheduleJob(lightSwitchingOnIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Switch on light and save date...');
		switchLight(1);
	});
	schedulers.push(swichOnLightJob);
	
	let lightSwitchingOffIntervalCroneCmd = croneConfigData.lightSwitchingOffIntervalCroneCmd;
	let swichOffLightJob = scheduler.scheduleJob(lightSwitchingOffIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Switch off light and save date...');
		switchLight(0);
	});
	schedulers.push(swichOffLightJob);
	
	
	let filterSwitchingOnIntervalCroneCmd = croneConfigData.filterSwitchingOnIntervalCroneCmd;
	let swichOnFilterJob = scheduler.scheduleJob(filterSwitchingOnIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Switch on filter and save date...');
		switchFilter(1);
	});
	schedulers.push(swichOnFilterJob);
	
	let filterSwitchingOffIntervalCroneCmd = croneConfigData.filterSwitchingOffIntervalCroneCmd;
	let swichOffFilterJob = scheduler.scheduleJob(filterSwitchingOffIntervalCroneCmd, function(){
		console.log('[' + logDateTime() + '| RPI:] Switch off filter and save date...');
		switchFilter(0);
	});
	schedulers.push(swichOffFilterJob);
	console.log('[' + logDateTime() + '| RPI:] Crone intervals set');
}

function registerSocketListeners(socket) {
	
	socket.on('connectedToSocket', function(data) {
		console.log('[' + logDateTime() + '| RPI:] socket ok! ', data);
	});
	
	socket.on('refreshData', function() {
		console.log('[' + logDateTime() + '| RPI:] refresh data request registerd');
		updateSensorsState(socket);
	});
	
	socket.on('feed', function() {
		console.log('[' + logDateTime() + '| RPI:] feeding request registerd');
		feed();
	});
	
	socket.on('light', function(newLightState) {
		console.log('[' + logDateTime() + '| RPI:] light swiching request registerd, newLightState: ', newLightState);
		switchLight(newLightState);
	});
	
	socket.on('filter', function(newFilterState) {
		console.log('[' + logDateTime() + '| RPI:] filter swiching request registerd, newFilterState: ', newFilterState);
		switchFilter(newFilterState);
	});
	
	socket.on('heater', function(newHeaterState) {
		console.log('[' + logDateTime() + '| RPI:] heater swiching request registerd, newHeaterState: ', newHeaterState);
		switchHeater(newHeaterState);
	});
	
	socket.on('updateConfigData', function(newHeaterState) {
		console.log('[' + logDateTime() + '| RPI:] update crone intervals request registerd,');
		refreshCroneIntervals();
	});
}

function updateSensorsState() {
	let sensorsData = {
		lastUpdateTime: new Date(),
		temperature: {
			value: raspberryService.getCurrentTemperatureWithoutPromise()
		},
		heater: {
			status: raspberryService.checkHeater()
		},
		feeding: {
			status: 0
		},
		light: {
			status: raspberryService.checkLight()
		},
		filter: {
			status: raspberryService.checkFilter()
		}
	}
	
	mainRepository.updateSensorsState(sensorsData).then(function (data){
		console.log('[' + logDateTime() + '| RPI:] sensors state updated!', data);
		socket.emit('refresh');
	}, function(err) {
		console.log(err);
	});
}

function feed() {
	let currentDate = new Date();
	let servoInput = 150;
    raspberryService.runServoMotor(servoInput).then(function(data) {
		updateSensorsState();
		mainRepository.saveFeedingTime(currentDate, data.status, {servoInput: servoInput}).then(function(data) {
			console.log('[' + logDateTime() + '| RPI:] Feeding time saved: ', data);
			socket.emit('feedingComplete', {});
		},function(err) {
			socket.emit('feedingError', {error: err});
			console.log('[' + logDateTime() + '| RPI:] ', err);
		});
	},function(err) {
		socket.emit('feedingError', {error: err});
		console.log('[' + logDateTime() + '| RPI:] ',err);
	});
}

function saveCurrentTemp() {
	let currentDate = new Date();
	raspberryService.getCurrentTemperature().then(function(data) {
		if (data.temperature >= configData.minTemperature) {
			switchHeater(0);
		} 
		
		if (data.temperature <= configData.maxTemperature) {
			switchHeater(1);
		}
		mainRepository
			.saveTemperatureMeisurement(currentDate, data.temperature, data.status, {})
			.then(function(data) {
				socket.emit('refresh');
				console.log('[' + logDateTime() + '| RPI:] Temperature saved: ', data);
			},function(err) {
				console.log('[' + logDateTime() + '| RPI:] ',err);
			});
	},function(err) {
		console.log(err);
	});
}

function switchLight(newLightState) {
	let currentDate = new Date();
	raspberryService.switchLight(newLightState).then(function(data) {
		updateSensorsState();
		mainRepository
			.saveLightSwitchingTime(currentDate, data.status, {newState: data.newState})
			.then(function(data) {
				console.log('[' + logDateTime() + '| RPI:] Light switching saved: ', data);
				socket.emit('lightSwitchingComplete', {});
			},function(err) {
				socket.emit('lightSwitchingError', {error: err});
				console.log('[' + logDateTime() + '| RPI:] ',err);
			});
	},function(err) {
		socket.emit('lightSwitchingError', {error: err});
		console.log('[' + logDateTime() + '| RPI:] ',err);
	});
}

function switchFilter(newFilterState) {
	let currentDate = new Date();
	raspberryService.switchFilter(newFilterState).then(function(data) {
		updateSensorsState();
		mainRepository
			.saveFilterSwitchingTime(currentDate, data.status, {newState: data.newState})
			.then(function(data) {
				console.log('[' + logDateTime() + '| RPI:] Filter switching saved: ', data);
				socket.emit('filterSwitchingComplete', {});
			},function(err) {
				socket.emit('filterSwitchingError', {error: err});
				console.log('[' + logDateTime() + '| RPI:] ',err);
			});
	},function(err) {
		socket.emit('filterSwitchingError', {error: err});
		console.log('[' + logDateTime() + '| RPI:] ',err);
	});
}

function switchHeater(newHeaterState) {
	let currentDate = new Date();
	raspberryService.switchHeater(newHeaterState).then(function(data) {
		updateSensorsState();
		mainRepository
			.saveHeaterSwitchingTime(currentDate, data.status, {newState: data.newState})
			.then(function(data) {
				console.log('[' + logDateTime() + '| RPI:] Heater switching saved: ', data);
				socket.emit('heaterSwitchingComplete', {});
			},function(err) {
				ocket.emit('heaterSwitchingError', {error: err});
				console.log('[' + logDateTime() + '| RPI:] ',err);
			});
	},function(err) {
		ocket.emit('heaterSwitchingError', {error: err});
		console.log('[' + logDateTime() + '| RPI:] ',err);
	});
}



console.log('[' + logDateTime() + '| RPI:] iAquarium rpi script started...');

function exitHandler(options, err) {
	
	if(err) {
		console.log(err);
	}
	
	if(options.exit) {
		console.log('exit');
		
		if(options.socket) {
			socket.emit('rpiDisconnected');
			socket.emit('refresh');
		}
		
		process.exit();
	}
}

function logDateTime() {
	return new Date().toLocaleTimeString() + ' ' + new Date().toLocaleDateString();
}

process.on('exit', exitHandler.bind(null, {exit: true, socket: socket}));
process.on('SIGTERM', exitHandler.bind(null, {exit: true, socket: socket}));
process.on('SIGINT', exitHandler.bind(null, {exit: true, socket: socket}));
process.on('SIGUSR1', exitHandler.bind(null, {exit: true, socket: socket}));
process.on('SIGUSR2', exitHandler.bind(null, {exit: true, socket: socket}));
process.on('uncaughtException', exitHandler.bind(null, {exit: false, socket: socket}));


