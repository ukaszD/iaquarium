Vue.use(VueResource);

let sensors = new Vue({
    el: '#sensors',
    data: {
        title: 'Sensors',
        sensors: {
            temperature: {
                value: '--',
                lastMeasurementTime: '--'
            },
            heater: {
                status: '--',
                time: '--',
                pending: false
            },
            feeding: {
                status: '--',
                time: '--',
            },
            light: {
                status: 0,
                time: '--',
                pending: false
            },
            filter: {
                status: '--',
                time: '--',
                pending: false
            }
        },
        heaterPending: false,
        lightPending: false,
        feedingPending: false,
        filterPending: false,
        configData: {},
        lastUpdateCounter: 0
    },
    methods: {
        getSensorsData: function () {
            this.$http.get('api/sensors').then((data) => {
                this.lastUpdateCounter = 0;
                this.sensors = data.body.sensors;
                this.sensors.temperature.lastMeasurementTime = moment(this.sensors.temperature.lastMeasurementTime).format('DD.MM.YY, HH:mm:ss');
                this.sensors.feeding.time = moment(this.sensors.feeding.time).format('DD.MM.YY, HH:mm:ss');
                this.sensors.light.time = moment(this.sensors.light.time).format('DD.MM.YY, HH:mm:ss');
                this.sensors.heater.time = moment(this.sensors.heater.time).format('DD.MM.YY, HH:mm:ss');
                this.sensors.filter.time = moment(this.sensors.filter.time).format('DD.MM.YY, HH:mm:ss');
            });
        },
        getConfigData: function () {
            this.$http.get('api/config').then((data) => {
                this.configData = data.body;
            });
        },
        setConfigData: function () {
            this.$http.post('api/config', this.configData).then((data) => {
                this.getConfigData();
            });
        },
        feed: function () {
            this.feedingPending = true;
            this.$http.put('api/sensors/feeding', {feed: true}).then((data) => {
                this.sensors.feeding.status = data.body;
                this.feedingPending = false;
                this.getSensorsData();
            }, (err) => {
                this.feedingPending = false;
                this.getSensorsData();
            });
        },
        toggleLight: function () {
            this.lightPending = true;
            this.$http.put('api/sensors/light', {newLightState: this.sensors.light.status? 0: 1}).then((data) => {
                this.sensors.light.status = data.body;
                this.lightPending = false;
                //this.getSensorsData();
            }, (err) => {
                this.lightPending = false;
                this.getSensorsData();
            });
        },
        toggleHeater: function () {
            this.heaterPending = true;
            this.$http.put('api/sensors/heater', {newHeaterState: this.sensors.heater.status? 0: 1}).then((data) => {
                this.sensors.heater.status = data.body;
                this.heaterPending = false;
                //this.getSensorsData();
            }, (err) => {
                this.heaterPending = false;
                this.getSensorsData();
            });
        },
        toggleFilter: function () {
            this.filterPending = true;
            this.$http.put('api/sensors/filter', {newFilterState: this.sensors.filter.status? 0: 1}).then((data) => {
                this.sensors.filter.status = data.body;
                this.filterPending = false;
                //this.getSensorsData();
            }, (err) => {
                this.filterPending = false;
                this.getSensorsData();
            });
        },
        refreshData: function () {
            this.getSensorsData();
            this.getConfigData();
        },

    }
});

sensors.getSensorsData();
sensors.getConfigData();

let socket = io('http://localhost:8080');

socket.on('connect', function(){
    socket.emit('appConnected');
});

socket.on('refreshApp', function(){
    sensors.getSensorsData();
    sensors.getConfigData();
    navbar.getSystemData();
    // tempChart.renderLineChart();
});

socket.on('feeding', function(){
    console.log('feeding');
});

socket.on('disconnect', function(){
    socket.emit('appDisconnected');
});

setInterval(function() {
    sensors.lastUpdateCounter++;
}, 1000);
