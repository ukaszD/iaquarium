Vue.use(VueResource);


let tempChart = new Vue({
    el: '#temp-chart',
    components: {
        'line-chart': {
            extends: VueChartJs.Line,
            props: ["data", "options"],
            mounted() {
                this.renderLineChart();
            },
            computed: {
                chartData: function() {
                    return this.data;
                }
            },
            methods: {
                renderLineChart: function() {
                    let self = this;
                    this.$http.get('api/temperatures').then((data) => {
                        let temperaturesData = data.body.lastTemperatureValues;
                        let temperatureChartData = {
                            values: [],
                            dates: []
                        };

                        for (let i = 0; i < temperaturesData.length; i++) {
                            temperatureChartData.values.push(temperaturesData[i].value);
                            temperaturesData[i].date = new Date(temperaturesData[i].date);
                            let formattedDate = temperaturesData[i].date.getHours() + ':'
                                + temperaturesData[i].date.getMinutes() + ':'
                                + temperaturesData[i].date.getSeconds() + ' '
                                + temperaturesData[i].date.getDate() + '.'
                                + (temperaturesData[i].date.getMonth() + 1) + '.'
                                + temperaturesData[i].date.getFullYear() + '';
                            temperatureChartData.dates.push(formattedDate);
                        }

                        self.renderChart({
                            labels: temperatureChartData.dates,
                            datasets: [
                                {
                                    label: 'Temperature',
                                    backgroundColor: '#f87979',
                                    data: temperatureChartData.values
                                }
                            ]
                        }, {responsive: true, maintainAspectRatio: false});
                    });
                }
            },
            watch: {
                data: function() {
                    this._chart.destroy();
                    //this.renderChart(this.data, this.options);
                    this.renderLineChart();
                }
            }

        }
    }
});