Vue.use(VueResource);

let navbar = new Vue({
    el: '#navbar',
    data: {
        system: {
            status: 'NOT WORKING'
        }
    },
    methods: {
        getSystemData: function () {
            this.$http.get('api/system').then((data) => {
                this.system = data.body.status? 'WORKING': 'NOT WORKING';
            });
        },
        restart: function () {

        }
    }
});

navbar.getSystemData();